export const COLORS = {
  primary: '#6B5DEF',
  secondary: '#6AAEF4',
  dark: '#2B2D33',
  grey: '#8F9096',
  lightGrey: '#BABEC6',
  extraLightGrey: '#EFF0F2',
  white: '#FFF'
};
